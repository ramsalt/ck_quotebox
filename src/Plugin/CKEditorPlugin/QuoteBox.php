<?php

/**
 * @file
 * Definition of \Drupal\ck_quotebox\Plugin\CKEditorPlugin\QuoteBox.
 */

namespace Drupal\ck_quotebox\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Ramsalt Quote" plugin.
 *
 * @CKEditorPlugin(
 *   id = "quotebox",
 *   label = @Translation("Ramsalt Quote")
 * )
 */
class QuoteBox extends CKEditorPluginBase {
  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
   */
  public function getFile() {
    return drupal_get_path('module', 'ck_quotebox') . '/js/plugins/quotebox/plugin.js';
  }

  /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
     */
    public function getButtons() {
      return [
        'quotebox' => [
          'label' => t('Quote Button'),
          'image' => drupal_get_path('module', 'ck_quotebox') . '/js/plugins/quotebox/icons/hidpi/quotebox.png'
        ]
      ];
    }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getConfig().
   */
  public function getConfig(Editor $editor) {
    return array();
  }

}
