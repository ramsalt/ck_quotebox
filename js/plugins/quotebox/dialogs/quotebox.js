CKEDITOR.dialog.add('quotebox', function (editor) {
  return {
    title: 'Add a Quotebox',
    minWidth: 200,
    minHeight: 100,
    contents: [
      {
        id: 'info',
        elements: [
          {
            id: 'quote',
            type: 'textarea',
            label: 'Quote',
            setup: function (element) {
              this.setValue(element.data.quote);
            },
            commit: function (element) {
              element.setData('quote', this.getValue());
            }
          },
          {
            id: 'author',
            type: 'text',
            label: 'Author',
            setup: function (element) {
              this.setValue(element.data.author);
            },
            commit: function (element) {
              element.setData('author', this.getValue());
            }
          },
          {
            id: 'align',
            type: 'select',
            label: 'Align',
            items: [
              [editor.lang.common.notSet, ''],
              [editor.lang.common.alignLeft, 'left'],
              [editor.lang.common.alignRight, 'right'],
              [editor.lang.common.alignCenter, 'center']
            ],
            setup: function (element) {
              this.setValue(element.data.align);
            },
            commit: function (element) {
              element.setData('align', this.getValue());
            }
          }
        ]
      }
    ]
  };
});