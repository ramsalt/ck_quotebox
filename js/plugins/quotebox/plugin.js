(function () {

  // Define basic widget elements.
  var widgetQuote = {
    selector: '.quotebox-content-inner',
    allowedContent: 'br strong em'
  };
  var widgetAuthor = {
    selector: '.quotebox-author-inner',
    allowedContent: ''
  };

  CKEDITOR.plugins.add('quotebox', {
    requires: 'widget,dialog',
    icons: 'quotebox',
    hidpi: true,

    init: function (editor) {
      // Register the editing dialog.
      CKEDITOR.dialog.add('quotebox', this.path + 'dialogs/quotebox.js');

      editor.addContentsCss(this.path + 'css/quotebox.css');

      // Register the quotebox widget.
      editor.widgets.add('quotebox', {
        allowedContent: 'div(!ramsalt-ckeditor-widgets-quotebox,align-left,align-right,align-center);' +
        'div(!quotebox-inner); div(!quotebox-content); div(!quotebox-content-inner); div(!quotebox-author); div(!quotebox-author-inner)',

        requiredContent: 'div(ramsalt-ckeditor-widgets-quotebox)',
        editables: {
          quote: widgetQuote,
          author: widgetAuthor
        },

        template: '<div class="ramsalt-ckeditor-widgets-quotebox">' +
        '<div class="quotebox-inner">' +
        '<div class="quotebox-content"><div class="quotebox-content-inner"><p>Quote Content…</p></div></div>' +
        '<div class="quotebox-author"><div class="quotebox-author-inner">Quote Author</div></div>' +
        '</div>' +
        '</div>',

        button: 'Create a quotebox',
        dialog: 'quotebox',

        upcast: function (element) {
          return element.name == 'div' && element.hasClass('ramsalt-ckeditor-widgets-quotebox');
        },

        init: function () {
          if (this.element.hasClass('align-left'))
            this.setData('align', 'left');
          if (this.element.hasClass('align-right'))
            this.setData('align', 'right');
          if (this.element.hasClass('align-center'))
            this.setData('align', 'center');

          var author = this.element.findOne('.quotebox-author-inner');
          if (author) {
            this.setData('author', author.getText());
          }

          var quote = this.element.findOne('.quotebox-content-inner');
          if (quote) {
            this.setData('quote', quote.getHtml());
          }
        },
        data: function () {
          this.element.removeClass('align-left');
          this.element.removeClass('align-right');
          this.element.removeClass('align-center');
          if (this.data.align) {
            this.element.addClass('align-' + this.data.align);
          }

          var author = this.element.findOne('.quotebox-author-inner');
          var author_data = this.data.author;
          if (author_data && author_data.length > 0) {
            if (author) {
              author.setText(author_data);
            } else {
              // No author markup is available in the DOM. Let's create it.
              var author = CKEDITOR.dom.element.createFromHtml('<div class="quotebox-author"><div class="quotebox-author-inner">' + author_data + '</div></div>');
              // And append it to the quote wrapper.
              var quote_wrapper = this.element.find('.quotebox-inner').getItem(0);
              quote_wrapper.append(author);
              this.initEditable('author', widgetAuthor);
            }
          } else {
            // Remove author markup, if no text is set.
            var author_wrapper = this.element.find('.quotebox-author').getItem(0);
            if (author_wrapper) {
              author_wrapper.remove();
            }
          }

          var quote = this.element.findOne('.quotebox-content-inner');
          if (quote) {
            quote.setHtml(this.data.quote);
          }
        }
      });

      editor.ui.addButton('quotebox', {
        label: 'Quotebox',
        command: 'quotebox',
        toolbar: 'insert',
        icon: this.path + "icons/" + (CKEDITOR.env.hidpi ? "hidpi/" : "") + "quotebox.png"
      });
    }
  });
})();
